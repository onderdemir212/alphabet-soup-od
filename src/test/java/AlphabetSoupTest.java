
import org.example.AlphabetSoup;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class AlphabetSoupTest {
    private AlphabetSoup alphabetSoup;

    @BeforeEach
    public void setup() {
        alphabetSoup = new AlphabetSoup();
    }

    @Test
    public void testFindWords() throws IOException {
        alphabetSoup.findWords();
        BufferedReader reader = new BufferedReader(new FileReader("output.txt"));
        ArrayList<String> output = new ArrayList<>();
        String line;
        while ((line = reader.readLine()) != null) {
            output.add(line);
        }
        reader.close();
        Assertions.assertEquals("HELLO 0:0 4:4", output.get(0));
        Assertions.assertEquals("GOOD 4:0 4:3", output.get(1));
        Assertions.assertEquals("BYE 1:3 1:1", output.get(2));
        Assertions.assertEquals("ONDER -1:-1 -1:-1", output.get(3));
        Assertions.assertEquals("BLN 3:2 3:4", output.get(4));
    }
}