package org.example;

import java.io.*;
import java.util.ArrayList;

public class AlphabetSoup {
    public void findWords() throws IOException {
        FileReader in = new FileReader("input.txt");
        BufferedWriter writer = new BufferedWriter(new FileWriter("output.txt"));
        BufferedReader reader = new BufferedReader(in);
        String line;
        String[] size;
        int numRows, numCols;

        // Read the size of the grid
        line = reader.readLine();
        size = line.split("x");
        numRows = Integer.parseInt(size[0]);
        numCols = Integer.parseInt(size[1]);

        // Read the grid
        char[][] grid = new char[numRows][numCols];
        for (int i = 0; i < numRows; i++) {
            line = reader.readLine();
            String[] chars = line.split(" ");
            for (int j = 0; j < numCols; j++) {
                grid[i][j] = chars[j].charAt(0);
            }
        }

        // Read the words to find
        ArrayList<String> words = new ArrayList<String>();
        while ((line = reader.readLine()) != null) {
            words.add(line);
        }

        // Find the words in the grid
        for (String word : words) {
            boolean found = false;
            int rowStart = -1, colStart = -1, rowEnd = -1, colEnd = -1;

            // Search for the word in the grid
            for (int i = 0; i < numRows; i++) {
                for (int j = 0; j < numCols; j++) {
                    if (grid[i][j] == word.charAt(0)) {
                        // Found the first character of the word checking all directions
                        for (int k = 0; k < 8; k++) {
                            int r = i, c = j, count = 0;
                            while (r >= 0 && r < numRows && c >= 0 && c < numCols
                                    && grid[r][c] == word.charAt(count)) {
                                count++;
                                if (count == word.length()) {
                                    found = true;
                                    rowStart = i;
                                    colStart = j;
                                    rowEnd = r;
                                    colEnd = c;
                                    break;
                                }
                                switch (k) {
                                    case 0:
                                        r--;
                                        break;
                                    case 1:
                                        r--;
                                        c++;
                                        break;
                                    case 2:
                                        c++;
                                        break;
                                    case 3:
                                        r++;
                                        c++;
                                        break;
                                    case 4:
                                        r++;
                                        break;
                                    case 5:
                                        r++;
                                        c--;
                                        break;
                                    case 6:
                                        c--;
                                        break;
                                    case 7:
                                        r--;
                                        c--;
                                        break;
                                }
                            }
                            if (found) {
                                break;
                            }
                        }
                    }
                    if (found) {
                        break;
                    }
                }
                if (found) {
                    break;
                }
            }
            // Write the word and its location to output file
            writer.write(word + " " + rowStart + ":" + colStart + " " + rowEnd + ":" + colEnd + "\n");
        }

        reader.close();
        writer.close();
    }
}